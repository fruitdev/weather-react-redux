import React, { Component } from 'react';

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      city: ''
    };
  }

  render() {
    return (
      <div className="col-lg-12 col-md-12" id="search-input">
        <input
          value={this.state.city}
          onChange={event => this.changeCity(event.target.value)}
          className="form-control"
          placeholder="Search city..." />
      </div>
    );
  }

  changeCity(city) {
    this.setState({
      city
    });
    this.props.onSearchCityChange(city);
  }
}
