import React, { Component } from 'react';

export default class CityListItem extends Component {
  render() {
    return (
      <li className="list-group-item">
        <h4 className="list-group-item-heading">{this.props.city.name} <span className="pull-right"><a href="#" onClick={(city) => this.props.removeCity(city)}><i className="fa fa-trash"></i></a></span></h4>
        <p>{this.props.city.info.main.temp} C</p>
      </li>
    );
  }
}
