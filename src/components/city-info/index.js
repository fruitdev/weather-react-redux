import React, { Component } from 'react';

export default class CityInfo extends Component {
  render() {
    if (!this.props.weatherInfo) {
      return (
        <div className="col-lg-8 col-md-8">
          <div className="well">
            <h4>Start typing the name of the city...</h4>
          </div>
        </div>
      );
    }

    return (
      <div className="col-lg-8 col-md-8">
        <div className="well">
          <h4>City: { this.props.weatherInfo.name }</h4>
          <p>Temp: { this.props.weatherInfo.main.temp } C</p>
          <p>{ this.props.weatherInfo.weather[0].description }</p>
          <button
            className="btn btn-primary"
            onClick={() => this.saveCity(this.props.weatherInfo)}>Save result</button>
        </div>
      </div>
    );
  }

  saveCity(info) {
    this.props.saveCity(info);
  }
}
