import React, { Component } from 'react';

import CityListItem from '../city-list-item';

export default class CityList extends Component {
  render() {
    const list = this.props.list.map(city => {
      return (
        <CityListItem
          removeCity={(coty) => this.props.removeCity(city)}
          city={city}
          key={city.name} />
      );
    });

    return (
      <div className="col-lg-4 col-md-4">
        <ul className="list-group">
          {list}
        </ul>
      </div>
    );
  }
}
