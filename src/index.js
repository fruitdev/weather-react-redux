import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// libs
import weather from 'openweathermap';
import _ from 'lodash';
import store from 'store';

weather.defaults({
  units: 'metric',
  lang: 'ru',
  mode: 'json',
  appid: 'ec6f6d31cbbd4a70207a854f166df01c'
});

// components
import Search from './components/search-city';
import CityInfo from './components/city-info';
import CityList from './components/city-list';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      city: '',
      weatherInfo: false,
      list: this.loadSavedCitys()
    };
  }

  loadSavedCitys() {
    let savedCitys = [];

    store.forEach(function(key, value) {
      if (key !== 'debug') {
        savedCitys.push({
          name: key,
          info: value
        });
      }
    });

    return savedCitys;
  }

  findCity(city) {
    weather.find({q: city}, (err, json) => {
      if (err) {
        console.log(err);
        this.setState({
          city: '',
          weatherInfo: false
        });
      } else {
        this.setState({
          city: city,
          weatherInfo: json.list[0]
        });
      }
    });
  }

  saveCity(info) {
    store.set(info.name, info);
    this.setState({
      list: this.loadSavedCitys()
    });
  }

  removeCity(city) {
    store.remove(city.name);
    this.setState({
      list: this.loadSavedCitys()
    });
  }

  render() {
    const findCity = _.debounce((city) => {this.findCity(city)}, 300);

    return (
      <div className="row">
        <Search onSearchCityChange={findCity} />
        <CityInfo weatherInfo={ this.state.weatherInfo } saveCity={(info) => { this.saveCity(info) }} />
        <CityList
          removeCity={(city) => this.removeCity(city)}
          list={this.state.list} />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector('#app'));
